<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/convert_bin', function (Request $request) {
    $requesData = $request->all();
    
    $input = $requesData['input'];

    $arrInput = str_split($input);

    $res = 0;
    foreach ($arrInput as $key => $val) {
        if($val == 1) {
            $res += 2**((count($arrInput)-1)-$key);
        }
    }

    return $res;
});

Route::post('/convert_des', function (Request $request) {
    $requesData = $request->all();
    
    $input = $requesData['input'];

    $res = "";
    while ($input != 0) {
        $res .= $input % 2;
        $input = floor($input / 2);
    }

    return strrev($res);
});
